import React from "react";
class Info extends React.Component {
    render(){
        return(
            <div>
                My name is {this.props.firstName} {this.props.lastName} and my favourite number is {this.props.favNumber}.
                <h2>{this.props.children}</h2>
            </div>
        )
    } 
}

export default Info;